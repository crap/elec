EESchema Schematic File Version 2
LIBS:crap
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:wsens-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date "14 oct 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2450BM15A0015 U201
U 1 1 542C63D6
P 3650 2800
F 0 "U201" H 4400 2300 60  0000 C CNN
F 1 "2450BM15A0015" H 3550 3150 60  0000 C CNN
F 2 "2450BM15A0015" H 3800 2300 60  0000 C CNN
F 3 "" H 3650 2600 60  0000 C CNN
	1    3650 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 542C63F9
P 4800 3250
F 0 "#PWR022" H 4800 3250 30  0001 C CNN
F 1 "GND" H 4800 3180 30  0001 C CNN
F 2 "" H 4800 3250 60  0000 C CNN
F 3 "" H 4800 3250 60  0000 C CNN
	1    4800 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2900 4800 3250
Wire Wire Line
	4800 3100 4650 3100
Wire Wire Line
	4800 3000 4650 3000
Connection ~ 4800 3100
Wire Wire Line
	4800 2900 4650 2900
Connection ~ 4800 3000
Text HLabel 3050 2900 0    60   Input ~ 0
RF_P
Text HLabel 3050 2800 0    60   Input ~ 0
RF_N
$Comp
L ANT_3216 A201
U 1 1 542C65DA
P 5850 2800
F 0 "A201" H 5950 2650 60  0000 C CNN
F 1 "ANT_3216" H 5900 3150 60  0000 C CNN
F 2 "ANT_3216" H 5950 2550 60  0000 C CNN
F 3 "" H 5850 2800 60  0000 C CNN
	1    5850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2650 5500 2650
$EndSCHEMATC
