EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:canservoitf-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date "18 sep 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2100 5050 1100 1200
U 5560C203
F0 "STM" 50
F1 "STM.sch" 50
F2 "CANRX" I L 2100 5550 60 
F3 "CANTX" O L 2100 5450 60 
F4 "POWER_EN" O R 3200 5200 60 
F5 "POWER_GOOD" I R 3200 5300 60 
F6 "PWM_0" O R 3200 5550 60 
F7 "PWM_1" O R 3200 5650 60 
F8 "PWM_2" O R 3200 5750 60 
F9 "PWM_3" O R 3200 5850 60 
F10 "PWM_4" O R 3200 5950 60 
F11 "PWM_5" O R 3200 6050 60 
$EndSheet
$Comp
L GND #PWR01
U 1 1 5561ECED
P 1450 3450
F 0 "#PWR01" H 1450 3450 30  0001 C CNN
F 1 "GND" H 1450 3380 30  0001 C CNN
F 2 "" H 1450 3450 60  0000 C CNN
F 3 "" H 1450 3450 60  0000 C CNN
	1    1450 3450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 5561ECFC
P 1450 2750
F 0 "#PWR02" H 1450 2840 20  0001 C CNN
F 1 "+5V" H 1450 2840 30  0000 C CNN
F 2 "" H 1450 2750 60  0000 C CNN
F 3 "" H 1450 2750 60  0000 C CNN
	1    1450 2750
	1    0    0    -1  
$EndComp
Text Label 1350 3150 0    60   ~ 0
CANL
Text Label 1350 3050 0    60   ~ 0
CANH
Text Label 2200 3050 2    60   ~ 0
CANH
Text Label 2200 2950 2    60   ~ 0
CANL
Text Label 4900 2550 2    60   ~ 0
POWER_EN
Text Label 4900 2700 2    60   ~ 0
POWER_GOOD
Text Label 4900 3900 2    60   ~ 0
PWM0
Text Label 4900 4050 2    60   ~ 0
PWM1
Text Label 4900 4200 2    60   ~ 0
PWM2
Text Label 4900 4350 2    60   ~ 0
PWM3
Text Label 4900 4500 2    60   ~ 0
PWM4
Text Label 4900 4650 2    60   ~ 0
PWM5
Text Label 3300 2900 0    60   ~ 0
LOCAL_CAN_TX
Text Label 3300 3000 0    60   ~ 0
LOCAL_CAN_RX
Wire Wire Line
	1450 2750 1450 2950
Wire Wire Line
	1450 2850 1350 2850
Wire Wire Line
	1450 2950 1350 2950
Connection ~ 1450 2850
Wire Wire Line
	1350 3350 1450 3350
Wire Wire Line
	1450 3250 1450 3450
Wire Wire Line
	1350 3250 1450 3250
Connection ~ 1450 3350
Wire Notes Line
	1650 3600 800  3600
Wire Wire Line
	2200 3050 2300 3050
Wire Wire Line
	2200 2950 2300 2950
Wire Wire Line
	3200 2900 3300 2900
Wire Wire Line
	3200 3000 3300 3000
Wire Wire Line
	4900 2550 5000 2550
Wire Wire Line
	5000 2700 4900 2700
Wire Wire Line
	4900 3900 5000 3900
Wire Wire Line
	4900 4050 5000 4050
Wire Wire Line
	4900 4200 5000 4200
Wire Wire Line
	4900 4350 5000 4350
Wire Wire Line
	4900 4500 5000 4500
Wire Wire Line
	4900 4650 5000 4650
Text Label 3300 5300 0    60   ~ 0
POWER_GOOD
Wire Wire Line
	3300 5300 3200 5300
Text Label 2000 5450 2    60   ~ 0
LOCAL_CAN_TX
Text Label 2000 5550 2    60   ~ 0
LOCAL_CAN_RX
Wire Wire Line
	2000 5450 2100 5450
Wire Wire Line
	2000 5550 2100 5550
Text Label 3300 5200 0    60   ~ 0
POWER_EN
Wire Wire Line
	3300 5200 3200 5200
Text Label 3300 6050 0    60   ~ 0
PWM5
Text Label 3300 5950 0    60   ~ 0
PWM4
Text Label 3300 5850 0    60   ~ 0
PWM3
Text Label 3300 5750 0    60   ~ 0
PWM2
Text Label 3300 5650 0    60   ~ 0
PWM1
Text Label 3300 5550 0    60   ~ 0
PWM0
Wire Wire Line
	3200 5550 3300 5550
Wire Wire Line
	3200 5650 3300 5650
Wire Wire Line
	3200 5750 3300 5750
Wire Wire Line
	3200 5850 3300 5850
Wire Wire Line
	3200 5950 3300 5950
Wire Wire Line
	3200 6050 3300 6050
$Sheet
S 5000 2200 1700 2850
U 55639B93
F0 "Isolation" 50
F1 "ISOLATION.sch" 50
F2 "POWER_EN" I L 5000 2550 60 
F3 "POWER_GOOD" O L 5000 2700 60 
F4 "PWM0" I L 5000 3900 60 
F5 "PWM1" I L 5000 4050 60 
F6 "PWM2" I L 5000 4200 60 
F7 "PWM3" I L 5000 4350 60 
F8 "PWM4" I L 5000 4500 60 
F9 "PWM5" I L 5000 4650 60 
F10 "P_POWER_EN" O R 6700 2550 60 
F11 "P_POWER_GOOD" I R 6700 2700 60 
F12 "P_PWM0" O R 6700 3900 60 
F13 "P_PWM1" O R 6700 4050 60 
F14 "P_PWM2" O R 6700 4200 60 
F15 "P_PWM3" O R 6700 4350 60 
F16 "P_PWM4" O R 6700 4500 60 
F17 "P_PWM5" O R 6700 4650 60 
$EndSheet
Text Label 6800 2550 0    60   ~ 0
P_POWER_EN
Wire Wire Line
	6800 2550 6700 2550
$Comp
L CONN_3 K101
U 1 1 55645C6F
P 9550 5400
F 0 "K101" V 9500 5400 50  0000 C CNN
F 1 "CONN_3" V 9600 5400 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 9500 5600 60  0000 C CNN
F 3 "" H 9550 5400 60  0000 C CNN
	1    9550 5400
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR03
U 1 1 55645D7F
P 9050 5500
F 0 "#PWR03" H 9050 5500 40  0001 C CNN
F 1 "AGND" H 9050 5430 50  0000 C CNN
F 2 "" H 9050 5500 60  0000 C CNN
F 3 "" H 9050 5500 60  0000 C CNN
	1    9050 5500
	0    1    1    0   
$EndComp
Text Label 9050 5300 2    60   ~ 0
P_PWM0
Wire Wire Line
	9050 5300 9200 5300
Wire Wire Line
	9050 5500 9200 5500
$Comp
L +6V #PWR04
U 1 1 55645F6D
P 8900 5400
F 0 "#PWR04" H 8900 5530 20  0001 C CNN
F 1 "+6V" H 8900 5500 30  0000 C CNN
F 2 "" H 8900 5400 60  0000 C CNN
F 3 "" H 8900 5400 60  0000 C CNN
	1    8900 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 5400 9200 5400
$Comp
L CONN_3 K102
U 1 1 55646128
P 10750 5400
F 0 "K102" V 10700 5400 50  0000 C CNN
F 1 "CONN_3" V 10800 5400 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 10600 5600 60  0000 C CNN
F 3 "" H 10750 5400 60  0000 C CNN
	1    10750 5400
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR05
U 1 1 5564612E
P 10250 5500
F 0 "#PWR05" H 10250 5500 40  0001 C CNN
F 1 "AGND" H 10250 5430 50  0000 C CNN
F 2 "" H 10250 5500 60  0000 C CNN
F 3 "" H 10250 5500 60  0000 C CNN
	1    10250 5500
	0    1    1    0   
$EndComp
Text Label 10250 5300 2    60   ~ 0
P_PWM1
Wire Wire Line
	10250 5300 10400 5300
Wire Wire Line
	10250 5500 10400 5500
$Comp
L +6V #PWR06
U 1 1 55646137
P 10100 5400
F 0 "#PWR06" H 10100 5530 20  0001 C CNN
F 1 "+6V" H 10100 5500 30  0000 C CNN
F 2 "" H 10100 5400 60  0000 C CNN
F 3 "" H 10100 5400 60  0000 C CNN
	1    10100 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10100 5400 10400 5400
$Comp
L CONN_3 K103
U 1 1 556461BC
P 9550 6000
F 0 "K103" V 9500 6000 50  0000 C CNN
F 1 "CONN_3" V 9600 6000 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 9500 6200 60  0000 C CNN
F 3 "" H 9550 6000 60  0000 C CNN
	1    9550 6000
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR07
U 1 1 556461C2
P 9050 6100
F 0 "#PWR07" H 9050 6100 40  0001 C CNN
F 1 "AGND" H 9050 6030 50  0000 C CNN
F 2 "" H 9050 6100 60  0000 C CNN
F 3 "" H 9050 6100 60  0000 C CNN
	1    9050 6100
	0    1    1    0   
$EndComp
Text Label 9050 5900 2    60   ~ 0
P_PWM2
Wire Wire Line
	9050 5900 9200 5900
Wire Wire Line
	9050 6100 9200 6100
$Comp
L +6V #PWR08
U 1 1 556461CB
P 8900 6000
F 0 "#PWR08" H 8900 6130 20  0001 C CNN
F 1 "+6V" H 8900 6100 30  0000 C CNN
F 2 "" H 8900 6000 60  0000 C CNN
F 3 "" H 8900 6000 60  0000 C CNN
	1    8900 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 6000 9200 6000
$Comp
L CONN_3 K104
U 1 1 556461D2
P 10750 6000
F 0 "K104" V 10700 6000 50  0000 C CNN
F 1 "CONN_3" V 10800 6000 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 10650 6200 60  0000 C CNN
F 3 "" H 10750 6000 60  0000 C CNN
	1    10750 6000
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR09
U 1 1 556461D8
P 10250 6100
F 0 "#PWR09" H 10250 6100 40  0001 C CNN
F 1 "AGND" H 10250 6030 50  0000 C CNN
F 2 "" H 10250 6100 60  0000 C CNN
F 3 "" H 10250 6100 60  0000 C CNN
	1    10250 6100
	0    1    1    0   
$EndComp
Text Label 10250 5900 2    60   ~ 0
P_PWM3
Wire Wire Line
	10250 5900 10400 5900
Wire Wire Line
	10250 6100 10400 6100
$Comp
L +6V #PWR010
U 1 1 556461E1
P 10100 6000
F 0 "#PWR010" H 10100 6130 20  0001 C CNN
F 1 "+6V" H 10100 6100 30  0000 C CNN
F 2 "" H 10100 6000 60  0000 C CNN
F 3 "" H 10100 6000 60  0000 C CNN
	1    10100 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10100 6000 10400 6000
$Comp
L CONN_3 K105
U 1 1 556466D4
P 9550 6600
F 0 "K105" V 9500 6600 50  0000 C CNN
F 1 "CONN_3" V 9600 6600 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 9500 6800 60  0000 C CNN
F 3 "" H 9550 6600 60  0000 C CNN
	1    9550 6600
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR011
U 1 1 556466DA
P 9050 6700
F 0 "#PWR011" H 9050 6700 40  0001 C CNN
F 1 "AGND" H 9050 6630 50  0000 C CNN
F 2 "" H 9050 6700 60  0000 C CNN
F 3 "" H 9050 6700 60  0000 C CNN
	1    9050 6700
	0    1    1    0   
$EndComp
Text Label 9050 6500 2    60   ~ 0
P_PWM4
Wire Wire Line
	9050 6500 9200 6500
Wire Wire Line
	9050 6700 9200 6700
$Comp
L +6V #PWR012
U 1 1 556466E3
P 8900 6600
F 0 "#PWR012" H 8900 6730 20  0001 C CNN
F 1 "+6V" H 8900 6700 30  0000 C CNN
F 2 "" H 8900 6600 60  0000 C CNN
F 3 "" H 8900 6600 60  0000 C CNN
	1    8900 6600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 6600 9200 6600
$Comp
L CONN_3 K106
U 1 1 556466EA
P 10750 6600
F 0 "K106" V 10700 6600 50  0000 C CNN
F 1 "CONN_3" V 10800 6600 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 10650 6800 60  0000 C CNN
F 3 "" H 10750 6600 60  0000 C CNN
	1    10750 6600
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR013
U 1 1 556466F0
P 10250 6700
F 0 "#PWR013" H 10250 6700 40  0001 C CNN
F 1 "AGND" H 10250 6630 50  0000 C CNN
F 2 "" H 10250 6700 60  0000 C CNN
F 3 "" H 10250 6700 60  0000 C CNN
	1    10250 6700
	0    1    1    0   
$EndComp
Text Label 10250 6500 2    60   ~ 0
P_PWM5
Wire Wire Line
	10250 6500 10400 6500
Wire Wire Line
	10250 6700 10400 6700
$Comp
L +6V #PWR014
U 1 1 556466F9
P 10100 6600
F 0 "#PWR014" H 10100 6730 20  0001 C CNN
F 1 "+6V" H 10100 6700 30  0000 C CNN
F 2 "" H 10100 6600 60  0000 C CNN
F 3 "" H 10100 6600 60  0000 C CNN
	1    10100 6600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10100 6600 10400 6600
Wire Notes Line
	8450 7000 8450 5000
$Sheet
S 2150 1000 950  900 
U 55647D5E
F0 "Power Logic" 50
F1 "Power_Logic.sch" 50
$EndSheet
$Sheet
S 8250 1050 1100 1050
U 556482CA
F0 "Power Power" 50
F1 "Power_Power.sch" 50
F2 "EN" I L 8250 1400 60 
$EndSheet
Wire Wire Line
	6750 2550 6750 2700
Wire Wire Line
	6750 2700 6700 2700
Connection ~ 6750 2550
Text Notes 6850 2700 0    60   ~ 0
Simply loop back\nthe power enable
Text Label 8150 1400 2    60   ~ 0
P_POWER_EN
Wire Wire Line
	8150 1400 8250 1400
$Comp
L CONN_2 P101
U 1 1 55649785
P 10650 1300
F 0 "P101" V 10600 1300 40  0000 C CNN
F 1 "CONN_2" V 10700 1300 40  0000 C CNN
F 2 "WE_CONN_3.81_vertical_2way" H 10550 1700 60  0000 C CNN
F 3 "" H 10650 1300 60  0000 C CNN
	1    10650 1300
	1    0    0    1   
$EndComp
$Comp
L AGND #PWR015
U 1 1 55649917
P 10200 1500
F 0 "#PWR015" H 10200 1500 40  0001 C CNN
F 1 "AGND" H 10200 1430 50  0000 C CNN
F 2 "" H 10200 1500 60  0000 C CNN
F 3 "" H 10200 1500 60  0000 C CNN
	1    10200 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 1500 10200 1400
Wire Wire Line
	10200 1400 10300 1400
$Comp
L +BATT #PWR016
U 1 1 556499F7
P 10200 1100
F 0 "#PWR016" H 10200 1050 20  0001 C CNN
F 1 "+BATT" H 10200 1200 30  0000 C CNN
F 2 "" H 10200 1100 60  0000 C CNN
F 3 "" H 10200 1100 60  0000 C CNN
	1    10200 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 1100 10200 1200
Wire Wire Line
	10200 1200 10300 1200
$Sheet
S 2300 2800 900  600 
U 5560CA91
F0 "CAN" 50
F1 "CAN.sch" 50
F2 "CANL" B L 2300 2950 60 
F3 "CANH" B L 2300 3050 60 
F4 "LOCAL_TX" I R 3200 2900 60 
F5 "LOCAL_RX" O R 3200 3000 60 
$EndSheet
Wire Notes Line
	8450 5000 11000 5000
Wire Notes Line
	11000 5000 11000 7000
Wire Notes Line
	11000 7000 8450 7000
Text Label 6800 3900 0    60   ~ 0
P_PWM0
Text Label 6800 4050 0    60   ~ 0
P_PWM1
Text Label 6800 4200 0    60   ~ 0
P_PWM2
Text Label 6800 4350 0    60   ~ 0
P_PWM3
Text Label 6800 4500 0    60   ~ 0
P_PWM4
Text Label 6800 4650 0    60   ~ 0
P_PWM5
Wire Wire Line
	6800 4650 6700 4650
Wire Wire Line
	6700 4500 6800 4500
Wire Wire Line
	6800 4350 6700 4350
Wire Wire Line
	6700 4200 6800 4200
Wire Wire Line
	6800 4050 6700 4050
Wire Wire Line
	6700 3900 6800 3900
$Comp
L CONN_6 P102
U 1 1 55688C5E
P 1000 3100
F 0 "P102" V 950 3100 60  0000 C CNN
F 1 "CONN_6" V 1050 3100 60  0000 C CNN
F 2 "TE_MICROMATCH_SMD_FEM_6PIN" H 750 3650 60  0000 C CNN
F 3 "" H 1000 3100 60  0000 C CNN
	1    1000 3100
	-1   0    0    1   
$EndComp
Wire Notes Line
	1650 3600 1650 2550
Wire Notes Line
	1650 2550 800  2550
Wire Notes Line
	800  2550 800  3600
$EndSCHEMATC
