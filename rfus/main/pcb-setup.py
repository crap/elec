#!/usr/bin/python

import sys
import math
import re

ox=100.
oy=100.

refs = [
'D301',
'D302',
'D303',
'D304',
'D305',
'D306',
'D307',
'D308',
'D309',
'D310',
'D311',
'D312',
'D313',
'D314',
'D315',
'D316',
'D317',
'D318',
'D319',
'D320',
'D321',
'D322',
'D323',
'D324'
]

cnt=len(refs)
r=35.
front=False

print 'Count: %d' % cnt

D = {}
for i in range(cnt):
    j = i if front else -i
    x = r * math.cos(2. * j * math.pi / cnt)
    y = r * math.sin(2. * j * math.pi / cnt)
    x = ox + x
    y = oy - y
    t = 270 + (360 * j / cnt)
    if not front:
        t += 180
    t %= 360
    D[refs[i]] = (x,y,t)

print D

cuts=[]
xcut = r
ycut = r * math.tan(math.pi / cnt)
for i in range(cnt):
    c = math.cos(2. * i * math.pi / cnt)
    s = math.sin(2. * i * math.pi / cnt)
    x = xcut * c - ycut * s
    y = xcut * s + ycut * c
    x = ox + x
    y = oy - y
    cuts.append((x,y))

fname = sys.argv[1]

f = open(fname, 'r')
lines = f.readlines()
f.close()

def replace(s, r, lgn):
    mo = r.match(s)
    index = 0
    res = ''
    if not (type(lgn) is list):
        lgn = [(1, lgn)]
    for gn in lgn:
        res += s[index:mo.start(gn[0])]
        res += gn[1]
        index = mo.end(gn[0])
    res += s[index:]
    return res

fp_text_ref = re.compile('    \(fp_text reference ([A-Z0-9]+) ')
at_re = re.compile('^.*\(at ([-0-9\.]+) ([-0-9\.]+)((?: [-0-9\.]+)?)\).*$')
layer_re = re.compile('^.*\(layer ([\.A-Za-z]+)\).*$')
layers_re = re.compile('^.*\(layers ([\.A-Za-z ]+)\).*$')
endmodule_re = re.compile('  \)')
pad_re = re.compile('    \(pad ([0-9]+) ')

def place_module(lines, idx, x, y, t):
    x = '%.3f' % x
    y = '%.3f' % y
    t = ' %.1f' % t
    # layer
    lines[idx] = replace(lines[idx], layer_re, 'F.Cu' if front else 'B.Cu')
    # position
    lines[idx+1] = replace(lines[idx+1], at_re, [(1,x),(2,y),(3,t)])
    # position & layer of text reference
    lines[idx+3] = replace(lines[idx+3], at_re, [(2,'2.5' if front else '-2.5'),(3,t)])
    lines[idx+3] = replace(lines[idx+3], layer_re, 'F.SilkS' if front else 'B.SilkS')
    # effects of text reference
    lines[idx+4] = '      (effects (font (size 1 1) (thickness 0.15))' + (')\n' if front else ' (justify mirror))\n')
    # angle of value ref
    lines[idx+6] = replace(lines[idx+6], at_re, [(2,'2.5' if front else '-2.5'),(3,t)])
    lines[idx+6] = replace(lines[idx+6], layer_re, 'F.SilkS' if front else 'B.SilkS')
    # effects of value ref
    lines[idx+7] = '      (effects (font (size 1 1) (thickness 0.15))' + (')\n' if front else ' (justify mirror))\n')
    k = idx + 7
    while not endmodule_re.match(lines[k]):
        # angle of pads
        mo = pad_re.match(lines[k])
        if mo:
            lines[k] = replace(lines[k], at_re, [(2,'0.75' if front else '-0.75'),(3,t)])
            side = front
            if int(mo.group(1)) in (1,2):
                side = not side
            lines[k+1] = replace(lines[k+1], layers_re, 'F.Cu F.Paste F.Mask' if side else 'B.Cu B.Paste B.Mask')
        k += 1

cuts_re = re.compile('\(layer Edge\.Cuts')
dwgs_re = re.compile('\(layer Dwgs\.User')

icuts = None

i = -1
while i + 1 < len(lines):
    i += 1
    l = lines[i]
    if cuts_re.search(l) or dwgs_re.search(l):
        lines.pop(i)
        i -= 1
        continue
    mo = fp_text_ref.match(l)
    if not mo:
        continue
    ref = mo.group(1)
    if not ref in D:
        continue
    x,y,t = D[ref]
    place_module(lines, i-3, x, y, t)

for i in range(cnt):
    lines.insert(len(lines) - 1,
            '  (gr_line (start %.2f %.2f) (end %.2f %.2f) (angle 90) (layer Edge.Cuts) (width 0.1))\n'
            % ( cuts[i-1][0], cuts[i-1][1], cuts[i][0], cuts[i][1]))

for i in [(-25,-25,-25,25),(-25,25,25,25),(25,25,25,-25),(25,-25,-25,-25)]:
    j = (ox + i[0], oy - i[1], ox + i[2], oy - i[3])
    lines.insert(len(lines) - 1, '  (gr_line (start %.2f %.2f) (end %.2f %.2f) (angle 90) (layer Dwgs.User) (width 0.1))\n' % j)

lines.insert(len(lines) - 1,'  (gr_circle (center %.2f %.2f) (end %.2f %.2f) (layer Dwgs.User) (width 0.1))\n'
        % (ox,oy,ox+40.,oy))
lines.insert(len(lines) - 1,'  (gr_circle (center %.2f %.2f) (end %.2f %.2f) (layer Dwgs.User) (width 0.1))\n'
        % (ox,oy,ox+37.5,oy))
#for i in [(-1,-1),(-1,1),(1,1),(1,-1)]:
#    j = (ox + 20 * i[0], oy - 20 * i[1], ox + 20 * i[0], oy - 20.5 * i[1])
#    lines.insert(len(lines) - 1,'  (gr_circle (center %.2f %.2f) (end %.2f %.2f) (layer Dwgs.User) (width 0.1))\n' % j)

if len(sys.argv) >= 3:
    fname = sys.argv[2]
f = open(fname, 'w')

for l in lines:
    f.write(l)
f.close()
