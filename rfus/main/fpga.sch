EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:rfus_main-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title ""
Date "3 oct 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 10800 1100 0    60   Output ~ 0
LEDS_BLANK
Text HLabel 10800 1250 0    60   Output ~ 0
LEDS_XLAT
Text HLabel 10800 1400 0    60   Output ~ 0
LEDS_SCLK
Text HLabel 10800 1550 0    60   Output ~ 0
LEDS_SIN
Text HLabel 10800 1700 0    60   Input ~ 0
LEDS_SOUT
Text HLabel 10800 2100 0    60   Input ~ 0
CLK
Text HLabel 10800 2250 0    60   Input ~ 0
SCLK
Text HLabel 10800 2400 0    60   Input ~ 0
MOSI
Text HLabel 10800 2550 0    60   Output ~ 0
MISO
Text HLabel 10800 2700 0    60   Input ~ 0
~CS
$EndSCHEMATC
