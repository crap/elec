EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:rfus_main-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date "3 oct 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1100 1100 0    60   Output ~ 0
RF_SCLK
Text HLabel 1100 1250 0    60   Output ~ 0
RF_MOSI
Text HLabel 1100 1400 0    60   Input ~ 0
RF_MISO
Text HLabel 1100 1550 0    60   Input ~ 0
~RF_CS
Text HLabel 1100 1700 0    60   Input ~ 0
RF_DIG2
Text HLabel 1100 1850 0    60   Input ~ 0
RF_IRQ
Text HLabel 1100 2000 0    60   Output ~ 0
~RF_RST
Text HLabel 1100 2150 0    60   Output ~ 0
RF_SLPTR
Text HLabel 1100 2500 0    60   Output ~ 0
FPGA_SCLK
Text HLabel 1100 2650 0    60   Output ~ 0
FPGA_MOSI
Text HLabel 1100 2800 0    60   Input ~ 0
FPGA_MISO
Text HLabel 1100 2950 0    60   Output ~ 0
~FPGA_CS
Text HLabel 1100 800  0    60   Input ~ 0
CLK
$Comp
L STM32F103CX U?
U 1 1 560FF115
P 5250 3300
F 0 "U?" H 5650 1550 60  0000 C CNN
F 1 "STM32F103CX" H 5250 4900 60  0000 C CNN
F 2 "~" H 5250 3300 60  0000 C CNN
F 3 "~" H 5250 3300 60  0000 C CNN
	1    5250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1100 1250 1100
Wire Wire Line
	1100 1250 1250 1250
Wire Wire Line
	1100 1400 1250 1400
Wire Wire Line
	1100 1550 1250 1550
Wire Wire Line
	1100 1700 1250 1700
Wire Wire Line
	1100 1850 1250 1850
Wire Wire Line
	1100 2000 1250 2000
Wire Wire Line
	1100 2150 1250 2150
Wire Wire Line
	1100 2500 1250 2500
Wire Wire Line
	1100 2650 1250 2650
Wire Wire Line
	1100 2800 1250 2800
Wire Wire Line
	1100 2950 1250 2950
Wire Wire Line
	1100 800  1250 800 
Text Label 1250 1100 0    60   ~ 0
RF_SCLK
Text Label 1250 1250 0    60   ~ 0
RF_MOSI
Text Label 1250 1400 0    60   ~ 0
RF_MISO
Text Label 1250 1550 0    60   ~ 0
~RF_CS
Text Label 1250 1700 0    60   ~ 0
RF_DIG2
Text Label 1250 1850 0    60   ~ 0
RF_IRQ
Text Label 1250 2000 0    60   ~ 0
~RF_RST
Text Label 1250 2150 0    60   ~ 0
RF_SLPTR
Text Label 1250 2500 0    60   ~ 0
FPGA_SCLK
Text Label 1250 2650 0    60   ~ 0
FPGA_MOSI
Text Label 1250 2800 0    60   ~ 0
FPGA_MISO
Text Label 1250 2950 0    60   ~ 0
~FPGA_CS
Text Label 1250 800  0    60   ~ 0
CLK
Wire Wire Line
	4300 3400 4200 3400
NoConn ~ 4300 3500
Text Label 4200 3400 2    60   ~ 0
CLK
Wire Wire Line
	6200 1700 6300 1700
Wire Wire Line
	6200 1900 6300 1900
Wire Wire Line
	6200 2000 6300 2000
Wire Wire Line
	6200 2100 6300 2100
Wire Wire Line
	6200 2200 6300 2200
Wire Wire Line
	6200 2300 6300 2300
Wire Wire Line
	6200 2400 6300 2400
Text Label 6300 1800 0    60   ~ 0
RF_IRQ
Text Label 6300 1900 0    60   ~ 0
RF_MOSI
Text Label 6300 2000 0    60   ~ 0
RF_MISO
Text Label 6300 2100 0    60   ~ 0
RF_SCLK
Text Label 6300 2600 0    60   ~ 0
~RF_CS
Text Label 6300 2300 0    60   ~ 0
RF_SLPTR
Text Label 6300 2400 0    60   ~ 0
RF_DIG2
Wire Wire Line
	6200 3400 6300 3400
Wire Wire Line
	6200 3500 6300 3500
Wire Wire Line
	6200 3600 6300 3600
Text Label 6300 3400 0    60   ~ 0
FPGA_IO0
Text Label 6300 3500 0    60   ~ 0
FPGA_IO1
Text Label 6300 3600 0    60   ~ 0
FPGA_IO2
Wire Wire Line
	6200 4700 6300 4700
Wire Wire Line
	6200 4800 6300 4800
Wire Wire Line
	6200 4900 6300 4900
Text Label 6300 4700 0    60   ~ 0
FPGA_SCLK
Text Label 6300 4800 0    60   ~ 0
FPGA_MISO
Text Label 6300 4900 0    60   ~ 0
FPGA_MOSI
Text Notes 6850 2000 0    60   ~ 0
USART2#0
Text Notes 6850 2350 0    60   ~ 0
TIM3#0CH1&2
Text Notes 6850 3500 0    60   ~ 0
TIM3#0 CH3-4 / EXTI0&2
Text Notes 6800 1800 0    60   ~ 0
EXTI1
Wire Wire Line
	6200 1800 6300 1800
Text Label 6300 2200 0    60   ~ 0
VBAT_FB
Text Notes 6850 2200 0    60   ~ 0
ADC CH5
Text Notes 6950 4800 0    60   ~ 0
SPI2#0
Wire Notes Line
	6900 4650 6900 4850
Wire Notes Line
	6800 1850 6800 2050
Wire Notes Line
	6800 2250 6800 2350
Wire Notes Line
	6800 3350 6800 3550
Wire Wire Line
	6200 3000 6300 3000
Wire Wire Line
	6200 3100 6300 3100
Text Label 6300 3000 0    60   ~ 0
SWDIO
Text Label 6300 3100 0    60   ~ 0
SWCLK
Wire Wire Line
	6200 2800 6300 2800
Wire Wire Line
	6200 2900 6300 2900
Text Label 6300 2800 0    60   ~ 0
CANRX
Text Label 6300 2900 0    60   ~ 0
CANTX
Wire Wire Line
	6200 4400 6300 4400
Wire Wire Line
	6200 4500 6300 4500
Text Label 6300 4400 0    60   ~ 0
DBGTX
Text Label 6300 4500 0    60   ~ 0
DBGRX
Wire Wire Line
	6200 4000 6300 4000
Wire Wire Line
	6200 4100 6300 4100
Wire Wire Line
	6200 3200 6300 3200
Wire Wire Line
	6200 3700 6300 3700
Wire Wire Line
	6200 3800 6300 3800
Wire Wire Line
	6200 3900 6300 3900
Wire Wire Line
	6200 4200 6300 4200
Wire Wire Line
	6200 4300 6300 4300
Text Label 6300 4200 0    60   ~ 0
EXT_SCL
Text Label 6300 4300 0    60   ~ 0
EXT_SDA
Text Label 6300 4000 0    60   ~ 0
EXT_TX
Text Label 6300 4100 0    60   ~ 0
EXT_RX
Text Label 6300 3700 0    60   ~ 0
EXT_SCLK
Text Label 6300 3800 0    60   ~ 0
EXT_MISO
Text Label 6300 3900 0    60   ~ 0
EXT_MOSI
Text Label 6300 3200 0    60   ~ 0
EXT_IO1
Wire Wire Line
	6200 4600 6300 4600
Text Label 6300 4600 0    60   ~ 0
FPGA_CS
Wire Notes Line
	6700 4350 6700 4450
Text Notes 6750 4450 0    60   ~ 0
USART3#0\n
Wire Notes Line
	6800 4150 6800 4250
Wire Notes Line
	6800 3950 6800 4050
Wire Notes Line
	6800 3650 6800 3850
Text Notes 6900 3800 0    60   ~ 0
SPI#1 / EXTI3-5
Text Notes 6900 4050 0    60   ~ 0
USART#1 / I2C1#0 / TIM4#0CH1-2
Text Notes 6900 4250 0    60   ~ 0
I2C1#1 / TIM4#0CH3-4
Wire Wire Line
	6200 2500 6300 2500
Wire Wire Line
	6200 2600 6300 2600
Wire Wire Line
	6200 2700 6300 2700
Text Label 6300 2700 0    60   ~ 0
EXT_IO0
Wire Notes Line
	6700 2750 6700 2850
Text Notes 6750 2850 0    60   ~ 0
CAN1#0
Text Label 6300 2500 0    60   ~ 0
~RF_RST
Text Label 6300 1700 0    60   ~ 0
EXT_ADC
Text Notes 6800 1700 0    60   ~ 0
ADC CH0
Wire Wire Line
	4300 3000 4200 3000
Wire Wire Line
	4300 3100 4200 3100
Wire Wire Line
	4300 2700 4200 2700
Text Label 4200 2700 2    60   ~ 0
EXT_IO2
Text Label 4200 3000 2    60   ~ 0
EXT_IO3
Text Label 4200 3100 2    60   ~ 0
EXT_IO4
$EndSCHEMATC
