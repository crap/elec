EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:rfus_main-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title ""
Date "3 oct 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2550 1450 1450 1100
U 55BB9E9C
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 10200 3400 800  600 
U 55B514B1
F0 "leds" 50
F1 "leds.sch" 50
F2 "RED[1..24]" I L 10200 3500 60 
F3 "GREEN[1..24]" I L 10200 3700 60 
F4 "BLUE[1..24]" I L 10200 3900 60 
$EndSheet
$Sheet
S 8200 3400 1200 600 
U 55859355
F0 "led_drivers" 50
F1 "led_drivers.sch" 50
F2 "BLANK" I L 8200 3500 60 
F3 "XLAT" I L 8200 3600 60 
F4 "SCLK" I L 8200 3700 60 
F5 "SIN" I L 8200 3800 60 
F6 "SOUT" O L 8200 3900 60 
F7 "RED[1..24]" O R 9400 3500 60 
F8 "BLUE[1..24]" O R 9400 3900 60 
F9 "GREEN[1..24]" O R 9400 3700 60 
$EndSheet
Wire Bus Line
	9400 3500 10200 3500
Wire Bus Line
	9400 3700 10200 3700
Wire Bus Line
	9400 3900 10200 3900
$Sheet
S 5500 3400 1900 3500
U 55BCC374
F0 "fpga" 50
F1 "fpga.sch" 50
F2 "LEDS_BLANK" O R 7400 3500 60 
F3 "LEDS_XLAT" O R 7400 3600 60 
F4 "LEDS_SCLK" O R 7400 3700 60 
F5 "LEDS_SIN" O R 7400 3800 60 
F6 "LEDS_SOUT" I R 7400 3900 60 
F7 "CLK" I L 5500 3500 60 
F8 "SCLK" I L 5500 3700 60 
F9 "MOSI" I L 5500 3800 60 
F10 "MISO" O L 5500 3900 60 
F11 "~CS" I L 5500 4000 60 
$EndSheet
$Sheet
S 1400 3400 700  1250
U 55BCCD44
F0 "radio" 50
F1 "radio.sch" 50
F2 "SCLK" I R 2100 3700 60 
F3 "MOSI" I R 2100 3800 60 
F4 "MISO" O R 2100 3900 60 
F5 "~CS" I R 2100 4000 60 
F6 "IRQ" O R 2100 4200 60 
F7 "CLKM" O R 2100 3500 60 
F8 "~RST" I R 2100 4600 60 
F9 "SLP_TR" I R 2100 4400 60 
F10 "DIG2" O R 2100 4300 60 
$EndSheet
Wire Wire Line
	7400 3500 8200 3500
Wire Wire Line
	7400 3700 8200 3700
Wire Wire Line
	7400 3900 8200 3900
$Sheet
S 3050 3400 1550 3050
U 55BCCC9D
F0 "mcu" 50
F1 "mcu.sch" 50
F2 "RF_SCLK" O L 3050 3700 60 
F3 "RF_MOSI" O L 3050 3800 60 
F4 "RF_MISO" I L 3050 3900 60 
F5 "~RF_CS" I L 3050 4000 60 
F6 "RF_DIG2" I L 3050 4300 60 
F7 "RF_IRQ" I L 3050 4200 60 
F8 "~RF_RST" O L 3050 4600 60 
F9 "RF_SLPTR" O L 3050 4400 60 
F10 "FPGA_SCLK" O R 4600 3700 60 
F11 "FPGA_MOSI" O R 4600 3800 60 
F12 "FPGA_MISO" I R 4600 3900 60 
F13 "~FPGA_CS" O R 4600 4000 60 
F14 "CLK" I L 3050 3500 60 
$EndSheet
Wire Wire Line
	2100 3500 3050 3500
Wire Wire Line
	3050 3700 2100 3700
Wire Wire Line
	2100 3800 3050 3800
Wire Wire Line
	3050 3900 2100 3900
Wire Wire Line
	2100 4000 3050 4000
Wire Wire Line
	3050 4200 2100 4200
Wire Wire Line
	2100 4300 3050 4300
Wire Wire Line
	3050 4400 2100 4400
Wire Wire Line
	2100 4600 3050 4600
Wire Wire Line
	4600 3700 5500 3700
Wire Wire Line
	5500 3800 4600 3800
Wire Wire Line
	4600 3900 5500 3900
Wire Wire Line
	5500 4000 4600 4000
Wire Wire Line
	2300 3500 2300 3200
Wire Wire Line
	2300 3200 5250 3200
Wire Wire Line
	5250 3200 5250 3500
Wire Wire Line
	5250 3500 5500 3500
Connection ~ 2300 3500
Wire Wire Line
	7400 3600 8200 3600
Wire Wire Line
	8200 3800 7400 3800
$EndSCHEMATC
