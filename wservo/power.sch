EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:wservo-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date "12 nov 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP1702 U201
U 1 1 543414AC
P 4750 2150
F 0 "U201" H 4850 1650 60  0000 C CNN
F 1 "MCP1702" H 4600 2100 60  0000 C CNN
F 2 "SOT23" H 4450 1650 60  0000 C CNN
F 3 "~" H 4750 2150 60  0000 C CNN
	1    4750 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR205
U 1 1 5434156A
P 4050 2600
F 0 "#PWR205" H 4050 2600 30  0001 C CNN
F 1 "GND" H 4050 2530 30  0001 C CNN
F 2 "" H 4050 2600 60  0000 C CNN
F 3 "" H 4050 2600 60  0000 C CNN
	1    4050 2600
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR203
U 1 1 54341579
P 4050 2250
F 0 "#PWR203" H 4050 2200 20  0001 C CNN
F 1 "+BATT" H 4050 2350 30  0000 C CNN
F 2 "" H 4050 2250 60  0000 C CNN
F 3 "" H 4050 2250 60  0000 C CNN
	1    4050 2250
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR204
U 1 1 54341588
P 5300 2250
F 0 "#PWR204" H 5300 2350 30  0001 C CNN
F 1 "VDD" H 5300 2360 30  0000 C CNN
F 2 "" H 5300 2250 60  0000 C CNN
F 3 "" H 5300 2250 60  0000 C CNN
	1    5300 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2250 4050 2350
Wire Wire Line
	4050 2350 4150 2350
Wire Wire Line
	4050 2600 4050 2500
Wire Wire Line
	4050 2500 4150 2500
Wire Wire Line
	5200 2350 5300 2350
Wire Wire Line
	5300 2350 5300 2250
$Comp
L +BATT #PWR201
U 1 1 5434164D
P 3450 2050
F 0 "#PWR201" H 3450 2000 20  0001 C CNN
F 1 "+BATT" H 3450 2150 30  0000 C CNN
F 2 "" H 3450 2050 60  0000 C CNN
F 3 "" H 3450 2050 60  0000 C CNN
	1    3450 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR207
U 1 1 54341653
P 3450 2750
F 0 "#PWR207" H 3450 2750 30  0001 C CNN
F 1 "GND" H 3450 2680 30  0001 C CNN
F 2 "" H 3450 2750 60  0000 C CNN
F 3 "" H 3450 2750 60  0000 C CNN
	1    3450 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR206
U 1 1 54341659
P 5800 2700
F 0 "#PWR206" H 5800 2700 30  0001 C CNN
F 1 "GND" H 5800 2630 30  0001 C CNN
F 2 "" H 5800 2700 60  0000 C CNN
F 3 "" H 5800 2700 60  0000 C CNN
	1    5800 2700
	1    0    0    -1  
$EndComp
$Comp
L C C202
U 1 1 54341667
P 3450 2400
F 0 "C202" H 3450 2500 40  0000 L CNN
F 1 "1u" H 3456 2315 40  0000 L CNN
F 2 "SM0805" H 3488 2250 30  0000 C CNN
F 3 "~" H 3450 2400 60  0000 C CNN
	1    3450 2400
	1    0    0    -1  
$EndComp
$Comp
L C C201
U 1 1 54341688
P 5800 2350
F 0 "C201" H 5800 2450 40  0000 L CNN
F 1 "1u" H 5806 2265 40  0000 L CNN
F 2 "SM0603" H 5838 2200 30  0000 C CNN
F 3 "~" H 5800 2350 60  0000 C CNN
	1    5800 2350
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR202
U 1 1 54341698
P 5800 2050
F 0 "#PWR202" H 5800 2150 30  0001 C CNN
F 1 "VDD" H 5800 2160 30  0000 C CNN
F 2 "" H 5800 2050 60  0000 C CNN
F 3 "" H 5800 2050 60  0000 C CNN
	1    5800 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2050 3450 2200
Wire Wire Line
	3450 2600 3450 2750
Wire Wire Line
	5800 2150 5800 2050
Wire Wire Line
	5800 2550 5800 2700
$EndSCHEMATC
