#!/bin/sh

if test $# -ne 1
then
    echo "usage: $0 pcbname"
    exit 1
fi

mv ${1}-F_Cu.gtl ${1}.gtl
mv ${1}-F_Mask.gts ${1}.gts
mv ${1}-F_SilkS.gto ${1}.gto

mv ${1}-B_Cu.gbl ${1}.gbl
mv ${1}-B_Mask.gbs ${1}.gbs
mv ${1}-B_SilkS.gbo ${1}.gbo

mv ${1}-Edge_Cuts.gbr ${1}.gml
