EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:canpump-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4950 2300 1250 2000
U 56BF329C
F0 "MCU" 60
F1 "MCU.sch" 60
F2 "CANRX" I L 4950 2500 60 
F3 "CANTX" O L 4950 2600 60 
F4 "PUMP0" O R 6200 2500 60 
F5 "PUMP1" O R 6200 2600 60 
F6 "PUMP2" O R 6200 2700 60 
F7 "PUMP3" O R 6200 2800 60 
F8 "PUMP4" O R 6200 2900 60 
F9 "PRES0" I L 4950 3300 60 
F10 "PRES1" I L 4950 3400 60 
F11 "PRES2" I L 4950 3500 60 
F12 "PRES3" I L 4950 3600 60 
F13 "PRES4" I L 4950 3700 60 
F14 "PVALVES" O R 6200 3300 60 
F15 "PGOOD" I R 6200 3450 60 
F16 "PUMPx" O R 6200 3000 60 
$EndSheet
$Sheet
S 8000 2350 900  750 
U 56BF32B6
F0 "pumps" 60
F1 "pumps.sch" 60
F2 "PUMP0" I L 8000 2500 60 
F3 "PUMP1" I L 8000 2600 60 
F4 "PUMP2" I L 8000 2700 60 
F5 "PUMP3" I L 8000 2800 60 
F6 "PUMP4" I L 8000 2900 60 
$EndSheet
$Sheet
S 3150 3200 1300 900 
U 56BF32D9
F0 "sensors" 60
F1 "sensors.sch" 60
F2 "PRES0" O R 4450 3300 60 
F3 "PRES1" O R 4450 3400 60 
F4 "PRES2" O R 4450 3500 60 
F5 "PRES3" O R 4450 3600 60 
F6 "PRES4" O R 4450 3700 60 
$EndSheet
$Sheet
S 3350 2350 1100 600 
U 56BF43E4
F0 "CAN" 60
F1 "CAN.sch" 60
F2 "CANL" B L 3350 2650 60 
F3 "CANH" B L 3350 2550 60 
F4 "TX" I R 4450 2600 60 
F5 "RX" O R 4450 2500 60 
$EndSheet
Text Notes 1250 1050 0    60   ~ 0
Bornier puissance:\nhttp://fr.farnell.com/lumberg/krmc-02/bornier-ci-2-voies/dp/1217302
$Sheet
S 6900 2300 750  1250
U 56C0B606
F0 "ISO" 60
F1 "ISO.sch" 60
F2 "IN0" I L 6900 2500 60 
F3 "IN1" I L 6900 2600 60 
F4 "IN2" I L 6900 2700 60 
F5 "IN3" I L 6900 2800 60 
F6 "IN4" I L 6900 2900 60 
F7 "IN5" I L 6900 3000 60 
F8 "IN6" I L 6900 3300 60 
F9 "OUT0" O R 7650 2500 60 
F10 "OUT1" O R 7650 2600 60 
F11 "OUT2" O R 7650 2700 60 
F12 "OUT3" O R 7650 2800 60 
F13 "OUT4" O R 7650 2900 60 
F14 "OUT5" O R 7650 3000 60 
F15 "OUT6" O R 7650 3300 60 
F16 "INB0" I R 7650 3450 60 
F17 "OUTB0" O L 6900 3450 60 
$EndSheet
$Sheet
S 8000 3300 700  400 
U 56C13397
F0 "power" 60
F1 "power.sch" 60
F2 "HALF" I L 8000 3400 60 
F3 "PGOOD" O L 8000 3500 60 
$EndSheet
Wire Wire Line
	6200 2500 6900 2500
Wire Wire Line
	6200 2600 6900 2600
Wire Wire Line
	6200 2700 6900 2700
Wire Wire Line
	6200 2800 6900 2800
Wire Wire Line
	6900 2900 6200 2900
Wire Wire Line
	4450 2500 4950 2500
Wire Wire Line
	4950 2600 4450 2600
$Comp
L CONN_01X06 P101
U 1 1 56C1D648
P 950 2450
F 0 "P101" H 950 2800 50  0000 C CNN
F 1 "CONN_01X06" V 1050 2450 50  0000 C CNN
F 2 "craplib:TE_MICROMATCH_SMD_FEM_6PIN" H 950 2450 60  0001 C CNN
F 3 "" H 950 2450 60  0000 C CNN
	1    950  2450
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 56C1DC83
P 1250 2800
F 0 "#PWR01" H 1250 2550 50  0001 C CNN
F 1 "GND" H 1250 2650 50  0000 C CNN
F 2 "" H 1250 2800 60  0000 C CNN
F 3 "" H 1250 2800 60  0000 C CNN
	1    1250 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2600 1250 2800
Wire Wire Line
	1250 2700 1150 2700
Wire Wire Line
	1150 2600 1250 2600
Connection ~ 1250 2700
$Comp
L +5V #PWR02
U 1 1 56C1DF0A
P 1250 2100
F 0 "#PWR02" H 1250 1950 50  0001 C CNN
F 1 "+5V" H 1250 2240 50  0000 C CNN
F 2 "" H 1250 2100 60  0000 C CNN
F 3 "" H 1250 2100 60  0000 C CNN
	1    1250 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2100 1250 2300
Wire Wire Line
	1250 2200 1150 2200
Wire Wire Line
	1250 2300 1150 2300
Connection ~ 1250 2200
Text Label 1300 2500 0    60   ~ 0
CANL
Text Label 1300 2400 0    60   ~ 0
CANH
Wire Wire Line
	1300 2400 1150 2400
Wire Wire Line
	1300 2500 1150 2500
Text Label 3250 2650 2    60   ~ 0
CANL
Text Label 3250 2550 2    60   ~ 0
CANH
Wire Wire Line
	3250 2650 3350 2650
Wire Wire Line
	3350 2550 3250 2550
$Sheet
S 2250 1600 500  350 
U 56C1F682
F0 "pow" 60
F1 "pow.sch" 60
$EndSheet
Wire Wire Line
	6200 3300 6900 3300
Wire Wire Line
	7650 3300 7850 3300
Wire Wire Line
	7850 3300 7850 3400
Wire Wire Line
	7850 3400 8000 3400
Wire Wire Line
	8000 2500 7650 2500
Wire Wire Line
	7650 2600 8000 2600
Wire Wire Line
	7650 2700 8000 2700
Wire Wire Line
	8000 2800 7650 2800
Wire Wire Line
	7650 2900 8000 2900
Wire Wire Line
	4450 3300 4950 3300
Wire Wire Line
	4950 3400 4450 3400
Wire Wire Line
	4450 3500 4950 3500
Wire Wire Line
	4950 3600 4450 3600
Wire Wire Line
	4450 3700 4950 3700
Wire Wire Line
	6200 3450 6900 3450
Wire Wire Line
	8000 3500 7800 3500
Wire Wire Line
	7800 3500 7800 3450
Wire Wire Line
	7800 3450 7650 3450
$Comp
L CONN_01X02 P102
U 1 1 56C59814
P 10000 3050
F 0 "P102" H 10000 3200 50  0000 C CNN
F 1 "CONN_01X02" V 10100 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 10000 3050 50  0001 C CNN
F 3 "" H 10000 3050 50  0000 C CNN
	1    10000 3050
	1    0    0    1   
$EndComp
$Comp
L GNDA #PWR03
U 1 1 56C59CA2
P 9700 3200
F 0 "#PWR03" H 9700 2950 50  0001 C CNN
F 1 "GNDA" H 9700 3050 50  0000 C CNN
F 2 "" H 9700 3200 50  0000 C CNN
F 3 "" H 9700 3200 50  0000 C CNN
	1    9700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 3200 9700 3100
Wire Wire Line
	9700 3100 9800 3100
Wire Wire Line
	9800 3000 9700 3000
Text Label 9700 3000 2    60   ~ 0
PUMPx
Text Label 7700 3000 0    60   ~ 0
PUMPx
Wire Wire Line
	7700 3000 7650 3000
Wire Wire Line
	6200 3000 6900 3000
$EndSCHEMATC
