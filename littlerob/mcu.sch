EESchema Schematic File Version 2
LIBS:littlerob-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crap
LIBS:littlerob-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EFR32MG_QFN48 U202
U 1 1 5AFDE867
P 5200 2600
F 0 "U202" H 4250 3650 60  0000 C CNN
F 1 "EFR32MG_QFN48" H 5200 2600 60  0000 C CNN
F 2 "lib:EFR32BG_QFN48" H 5200 2600 60  0001 C CNN
F 3 "" H 5200 2600 60  0000 C CNN
	1    5200 2600
	1    0    0    -1  
$EndComp
Text HLabel 4750 1550 1    60   BiDi ~ 0
SCL
Text HLabel 4650 1550 1    60   Output ~ 0
SDA
Text HLabel 8350 3550 2    60   Input ~ 0
E0_B
Text HLabel 8350 3650 2    60   Input ~ 0
E0_A
Text HLabel 8350 3800 2    60   Input ~ 0
E1_B
Text HLabel 8350 3900 2    60   Input ~ 0
E1_A
$Comp
L JTAGCON P201
U 1 1 5B0343E4
P 1350 1350
F 0 "P201" H 1850 1000 60  0000 C CNN
F 1 "JTAGCON" H 1100 1750 60  0000 C CNN
F 2 "lib:BM06B-SRSS-TB" H 1800 1550 60  0001 C CNN
F 3 "" H 1800 1550 60  0000 C CNN
	1    1350 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR030
U 1 1 5B034413
P 750 1700
F 0 "#PWR030" H 750 1450 50  0001 C CNN
F 1 "GND" H 750 1550 50  0000 C CNN
F 2 "" H 750 1700 50  0001 C CNN
F 3 "" H 750 1700 50  0001 C CNN
	1    750  1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5B034486
P 2150 1700
F 0 "#PWR031" H 2150 1450 50  0001 C CNN
F 1 "GND" H 2150 1550 50  0000 C CNN
F 2 "" H 2150 1700 50  0001 C CNN
F 3 "" H 2150 1700 50  0001 C CNN
	1    2150 1700
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR032
U 1 1 5B0344B5
P 2150 1000
F 0 "#PWR032" H 2150 850 50  0001 C CNN
F 1 "+3.3V" H 2150 1140 50  0000 C CNN
F 2 "" H 2150 1000 50  0001 C CNN
F 3 "" H 2150 1000 50  0001 C CNN
	1    2150 1000
	1    0    0    -1  
$EndComp
Text Label 2150 1200 0    60   ~ 0
DBG_TX
Text Label 2150 1300 0    60   ~ 0
DBG_RX
Text Label 2150 1400 0    60   ~ 0
SWDIO
Text Label 2150 1500 0    60   ~ 0
SWCLK
Text HLabel 5900 3800 2    60   Output ~ 0
SDA_EXT
Text HLabel 5900 3700 2    60   BiDi ~ 0
SCL_EXT
Text HLabel 5900 4000 2    60   Output ~ 0
M0_PWM1
Text HLabel 5900 3900 2    60   Output ~ 0
M0_PWM2
Text HLabel 3950 2450 0    60   Output ~ 0
M1_PWM1
Text HLabel 3950 2550 0    60   Output ~ 0
M1_PWM2
Text HLabel 5150 1550 1    60   Input ~ 0
GYRO_INT
Text Label 4000 2050 2    60   ~ 0
SWCLK
Text Label 4000 2150 2    60   ~ 0
SWDIO
Text Label 4000 2250 2    60   ~ 0
DBG_RX
Text Label 4000 2350 2    60   ~ 0
DBG_TX
$Comp
L Crystal_GND24 Y202
U 1 1 5B031193
P 3100 3050
F 0 "Y202" H 3225 3250 50  0000 L CNN
F 1 "38.4MHz" V 3400 2950 50  0000 L CNN
F 2 "Crystals:Crystal_SMD_2016-4pin_2.0x1.6mm" H 3100 3050 50  0001 C CNN
F 3 "" H 3100 3050 50  0001 C CNN
	1    3100 3050
	0    1    1    0   
$EndComp
$Comp
L GND #PWR033
U 1 1 5B031250
P 3350 3100
F 0 "#PWR033" H 3350 2850 50  0001 C CNN
F 1 "GND" H 3350 2950 50  0000 C CNN
F 2 "" H 3350 3100 50  0001 C CNN
F 3 "" H 3350 3100 50  0001 C CNN
	1    3350 3100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR034
U 1 1 5B03129F
P 2850 3100
F 0 "#PWR034" H 2850 2850 50  0001 C CNN
F 1 "GND" H 2850 2950 50  0000 C CNN
F 2 "" H 2850 3100 50  0001 C CNN
F 3 "" H 2850 3100 50  0001 C CNN
	1    2850 3100
	1    0    0    -1  
$EndComp
$Comp
L Crystal Y201
U 1 1 5B03163C
P 6650 2000
F 0 "Y201" H 6650 2150 50  0000 C CNN
F 1 "32kHz" H 6650 1850 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_3215-2pin_3.2x1.5mm" H 6650 2000 50  0001 C CNN
F 3 "" H 6650 2000 50  0001 C CNN
	1    6650 2000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR035
U 1 1 5B0318F9
P 4750 3800
F 0 "#PWR035" H 4750 3550 50  0001 C CNN
F 1 "GND" H 4750 3650 50  0000 C CNN
F 2 "" H 4750 3800 50  0001 C CNN
F 3 "" H 4750 3800 50  0001 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
$Comp
L L_Small L202
U 1 1 5B031B54
P 4950 4050
F 0 "L202" H 4980 4090 50  0000 L CNN
F 1 "1.8n" H 4980 4010 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 4950 4050 50  0001 C CNN
F 3 "" H 4950 4050 50  0001 C CNN
	1    4950 4050
	0    1    1    0   
$EndComp
$Comp
L L_Small L201
U 1 1 5B031BF4
P 4550 4050
F 0 "L201" H 4580 4090 50  0000 L CNN
F 1 "3.0n" H 4580 4010 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 4550 4050 50  0001 C CNN
F 3 "" H 4550 4050 50  0001 C CNN
	1    4550 4050
	0    1    -1   0   
$EndComp
$Comp
L Conn_01x01 J201
U 1 1 5B031C60
P 4150 4050
F 0 "J201" H 4150 4150 50  0000 C CNN
F 1 "Conn_01x01" H 4150 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.00mm" H 4150 4050 50  0001 C CNN
F 3 "" H 4150 4050 50  0001 C CNN
	1    4150 4050
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C209
U 1 1 5B031D30
P 4750 4250
F 0 "C209" H 4760 4320 50  0000 L CNN
F 1 "2.0p" H 4760 4170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4750 4250 50  0001 C CNN
F 3 "" H 4750 4250 50  0001 C CNN
	1    4750 4250
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C208
U 1 1 5B031D74
P 4400 4250
F 0 "C208" H 4410 4320 50  0000 L CNN
F 1 "1.0p" H 4410 4170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0001 C CNN
	1    4400 4250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 5B031E42
P 4600 4500
F 0 "#PWR036" H 4600 4250 50  0001 C CNN
F 1 "GND" H 4600 4350 50  0000 C CNN
F 2 "" H 4600 4500 50  0001 C CNN
F 3 "" H 4600 4500 50  0001 C CNN
	1    4600 4500
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C201
U 1 1 5B0321B0
P 5350 1150
F 0 "C201" H 5360 1220 50  0000 L CNN
F 1 "1u" H 5360 1070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5350 1150 50  0001 C CNN
F 3 "" H 5350 1150 50  0001 C CNN
	1    5350 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 5B032294
P 5350 950
F 0 "#PWR037" H 5350 700 50  0001 C CNN
F 1 "GND" H 5350 800 50  0000 C CNN
F 2 "" H 5350 950 50  0001 C CNN
F 3 "" H 5350 950 50  0001 C CNN
	1    5350 950 
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR038
U 1 1 5B0324BC
P 5750 1450
F 0 "#PWR038" H 5750 1200 50  0001 C CNN
F 1 "GND" H 5750 1300 50  0000 C CNN
F 2 "" H 5750 1450 50  0001 C CNN
F 3 "" H 5750 1450 50  0001 C CNN
	1    5750 1450
	-1   0    0    1   
$EndComp
NoConn ~ 4150 3150
$Comp
L +3.3V #PWR039
U 1 1 5B032996
P 4050 2850
F 0 "#PWR039" H 4050 2700 50  0001 C CNN
F 1 "+3.3V" H 4050 2990 50  0000 C CNN
F 2 "" H 4050 2850 50  0001 C CNN
F 3 "" H 4050 2850 50  0001 C CNN
	1    4050 2850
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR040
U 1 1 5B032A2A
P 5800 4500
F 0 "#PWR040" H 5800 4350 50  0001 C CNN
F 1 "+3.3V" H 5800 4640 50  0000 C CNN
F 2 "" H 5800 4500 50  0001 C CNN
F 3 "" H 5800 4500 50  0001 C CNN
	1    5800 4500
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR041
U 1 1 5B032D10
P 5250 1450
F 0 "#PWR041" H 5250 1300 50  0001 C CNN
F 1 "+3.3V" H 5250 1590 50  0000 C CNN
F 2 "" H 5250 1450 50  0001 C CNN
F 3 "" H 5250 1450 50  0001 C CNN
	1    5250 1450
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR042
U 1 1 5B032DA9
P 5450 1450
F 0 "#PWR042" H 5450 1300 50  0001 C CNN
F 1 "+3.3V" H 5450 1590 50  0000 C CNN
F 2 "" H 5450 1450 50  0001 C CNN
F 3 "" H 5450 1450 50  0001 C CNN
	1    5450 1450
	1    0    0    -1  
$EndComp
NoConn ~ 5650 1550
$Comp
L L_Small L203
U 1 1 5B033477
P 5650 4500
F 0 "L203" H 5680 4540 50  0000 L CNN
F 1 "L_Small" H 5680 4460 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5650 4500 50  0001 C CNN
F 3 "" H 5650 4500 50  0001 C CNN
	1    5650 4500
	0    -1   -1   0   
$EndComp
Text Notes 3700 4050 0    60   ~ 0
antenna
$Comp
L C_Small C214
U 1 1 5B033CCA
P 5450 4650
F 0 "C214" H 5460 4720 50  0000 L CNN
F 1 "10p" H 5460 4570 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5450 4650 50  0001 C CNN
F 3 "" H 5450 4650 50  0001 C CNN
	1    5450 4650
	1    0    0    -1  
$EndComp
$Comp
L C_Small C213
U 1 1 5B033D4C
P 5300 4650
F 0 "C213" H 5310 4720 50  0000 L CNN
F 1 "100p" H 5200 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5300 4650 50  0001 C CNN
F 3 "" H 5300 4650 50  0001 C CNN
	1    5300 4650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR043
U 1 1 5B033EEB
P 5450 4850
F 0 "#PWR043" H 5450 4600 50  0001 C CNN
F 1 "GND" H 5450 4700 50  0000 C CNN
F 2 "" H 5450 4850 50  0001 C CNN
F 3 "" H 5450 4850 50  0001 C CNN
	1    5450 4850
	-1   0    0    -1  
$EndComp
$Comp
L C C202
U 1 1 5B0345C4
P 950 3500
F 0 "C202" H 975 3600 50  0000 L CNN
F 1 "1u" H 975 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 988 3350 50  0001 C CNN
F 3 "" H 950 3500 50  0001 C CNN
	1    950  3500
	1    0    0    -1  
$EndComp
$Comp
L C C203
U 1 1 5B03460E
P 1200 3500
F 0 "C203" H 1225 3600 50  0000 L CNN
F 1 "100n" H 1225 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1238 3350 50  0001 C CNN
F 3 "" H 1200 3500 50  0001 C CNN
	1    1200 3500
	1    0    0    -1  
$EndComp
$Comp
L C C204
U 1 1 5B034654
P 1450 3500
F 0 "C204" H 1475 3600 50  0000 L CNN
F 1 "1u" H 1475 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1488 3350 50  0001 C CNN
F 3 "" H 1450 3500 50  0001 C CNN
	1    1450 3500
	1    0    0    -1  
$EndComp
$Comp
L C C205
U 1 1 5B0346A5
P 1700 3500
F 0 "C205" H 1725 3600 50  0000 L CNN
F 1 "100n" H 1725 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1738 3350 50  0001 C CNN
F 3 "" H 1700 3500 50  0001 C CNN
	1    1700 3500
	1    0    0    -1  
$EndComp
$Comp
L C C206
U 1 1 5B0346F5
P 1950 3500
F 0 "C206" H 1975 3600 50  0000 L CNN
F 1 "1u" H 1975 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1988 3350 50  0001 C CNN
F 3 "" H 1950 3500 50  0001 C CNN
	1    1950 3500
	1    0    0    -1  
$EndComp
$Comp
L C C207
U 1 1 5B03475B
P 2200 3500
F 0 "C207" H 2225 3600 50  0000 L CNN
F 1 "100n" H 2225 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2238 3350 50  0001 C CNN
F 3 "" H 2200 3500 50  0001 C CNN
	1    2200 3500
	1    0    0    -1  
$EndComp
$Comp
L C C210
U 1 1 5B0348C3
P 950 4500
F 0 "C210" H 975 4600 50  0000 L CNN
F 1 "1u" H 975 4400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 988 4350 50  0001 C CNN
F 3 "" H 950 4500 50  0001 C CNN
	1    950  4500
	1    0    0    -1  
$EndComp
$Comp
L C C211
U 1 1 5B0348C9
P 1200 4500
F 0 "C211" H 1225 4600 50  0000 L CNN
F 1 "100n" H 1225 4400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1238 4350 50  0001 C CNN
F 3 "" H 1200 4500 50  0001 C CNN
	1    1200 4500
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR044
U 1 1 5B03491C
P 950 3200
F 0 "#PWR044" H 950 3050 50  0001 C CNN
F 1 "+3.3V" H 950 3340 50  0000 C CNN
F 2 "" H 950 3200 50  0001 C CNN
F 3 "" H 950 3200 50  0001 C CNN
	1    950  3200
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR045
U 1 1 5B034C4B
P 950 4250
F 0 "#PWR045" H 950 4100 50  0001 C CNN
F 1 "+3.3V" H 950 4390 50  0000 C CNN
F 2 "" H 950 4250 50  0001 C CNN
F 3 "" H 950 4250 50  0001 C CNN
	1    950  4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR046
U 1 1 5B034CCD
P 950 3800
F 0 "#PWR046" H 950 3550 50  0001 C CNN
F 1 "GND" H 950 3650 50  0000 C CNN
F 2 "" H 950 3800 50  0001 C CNN
F 3 "" H 950 3800 50  0001 C CNN
	1    950  3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1500 750  1700
Wire Wire Line
	750  1600 850  1600
Wire Wire Line
	750  1500 850  1500
Connection ~ 750  1600
Wire Wire Line
	2150 1700 2150 1600
Wire Wire Line
	2150 1600 2050 1600
Wire Wire Line
	2150 1000 2150 1100
Wire Wire Line
	2150 1100 2050 1100
Wire Wire Line
	2150 1200 2050 1200
Wire Wire Line
	2050 1300 2150 1300
Wire Wire Line
	2150 1400 2050 1400
Wire Wire Line
	2050 1500 2150 1500
Wire Wire Line
	4000 2050 4150 2050
Wire Wire Line
	4000 2150 4150 2150
Wire Wire Line
	4000 2250 4150 2250
Wire Wire Line
	4000 2350 4150 2350
Wire Wire Line
	3100 2900 3100 2850
Wire Wire Line
	3100 2850 3450 2850
Wire Wire Line
	3450 2850 3450 2950
Wire Wire Line
	3450 2950 4150 2950
Wire Wire Line
	3100 3200 3100 3250
Wire Wire Line
	3100 3250 3450 3250
Wire Wire Line
	3450 3250 3450 3050
Wire Wire Line
	3450 3050 4150 3050
Wire Wire Line
	3300 3050 3350 3050
Wire Wire Line
	3350 3050 3350 3100
Wire Wire Line
	2850 3100 2850 3050
Wire Wire Line
	2850 3050 2900 3050
Wire Wire Line
	6250 2150 6850 2150
Wire Wire Line
	6250 2050 6350 2050
Wire Wire Line
	6350 2050 6350 1750
Wire Wire Line
	6350 1750 6850 1750
Wire Wire Line
	6650 1750 6650 1850
Wire Wire Line
	4750 3650 4750 3800
Wire Wire Line
	4750 3750 4950 3750
Wire Wire Line
	4850 3750 4850 3650
Connection ~ 4750 3750
Wire Wire Line
	4950 3750 4950 3650
Connection ~ 4850 3750
Wire Wire Line
	5050 3650 5050 4050
Wire Wire Line
	4350 4050 4450 4050
Wire Wire Line
	4650 4050 4850 4050
Wire Wire Line
	4400 4150 4400 4050
Connection ~ 4400 4050
Wire Wire Line
	4750 4150 4750 4050
Connection ~ 4750 4050
Wire Wire Line
	4750 4350 4750 4450
Wire Wire Line
	4750 4450 4400 4450
Wire Wire Line
	4600 4450 4600 4500
Wire Wire Line
	4400 4450 4400 4350
Connection ~ 4600 4450
Wire Wire Line
	5350 1250 5350 1550
Wire Wire Line
	5350 950  5350 1050
Wire Wire Line
	5750 1450 5750 1550
Wire Wire Line
	4050 2850 4150 2850
Wire Wire Line
	5150 3650 5150 4500
Wire Wire Line
	5450 1450 5450 1550
Wire Wire Line
	5250 1450 5250 1550
Wire Wire Line
	5450 1500 5550 1500
Wire Wire Line
	5550 1500 5550 1550
Connection ~ 5450 1500
Wire Wire Line
	5750 4500 5800 4500
Wire Wire Line
	5150 4500 5550 4500
Wire Wire Line
	5450 4500 5450 4550
Wire Wire Line
	5300 4500 5300 4550
Connection ~ 5450 4500
Wire Wire Line
	5450 4850 5450 4750
Wire Wire Line
	5450 4800 5300 4800
Wire Wire Line
	5300 4800 5300 4750
Connection ~ 5450 4800
Connection ~ 5300 4500
Wire Wire Line
	950  3200 950  3350
Wire Wire Line
	950  3250 2200 3250
Wire Wire Line
	1200 3250 1200 3350
Connection ~ 950  3250
Wire Wire Line
	1450 3250 1450 3350
Connection ~ 1200 3250
Wire Wire Line
	1700 3250 1700 3350
Connection ~ 1450 3250
Wire Wire Line
	1950 3250 1950 3350
Connection ~ 1700 3250
Wire Wire Line
	2200 3250 2200 3350
Connection ~ 1950 3250
Wire Wire Line
	950  3800 950  3650
Wire Wire Line
	950  3750 2200 3750
Wire Wire Line
	1200 3750 1200 3650
Connection ~ 950  3750
Wire Wire Line
	1450 3750 1450 3650
Connection ~ 1200 3750
Wire Wire Line
	2200 3750 2200 3650
Connection ~ 1450 3750
Wire Wire Line
	1950 3650 1950 3750
Connection ~ 1950 3750
Wire Wire Line
	1700 3650 1700 3750
Connection ~ 1700 3750
Wire Wire Line
	950  4250 950  4350
Connection ~ 950  4300
Wire Wire Line
	1200 4300 1200 4350
Wire Wire Line
	950  4650 950  4850
Wire Wire Line
	1200 4650 1200 4750
$Comp
L GND #PWR047
U 1 1 5B0353BC
P 950 4850
F 0 "#PWR047" H 950 4600 50  0001 C CNN
F 1 "GND" H 950 4700 50  0000 C CNN
F 2 "" H 950 4850 50  0001 C CNN
F 3 "" H 950 4850 50  0001 C CNN
	1    950  4850
	1    0    0    -1  
$EndComp
Connection ~ 950  4750
Text HLabel 6450 3150 2    60   Output ~ 0
RS485_TX
Text HLabel 6450 3050 2    60   Input ~ 0
RS485_RX
Text HLabel 6450 2950 2    60   Input ~ 0
EXT_INT
$Comp
L R R202
U 1 1 5B04049D
P 8050 3550
F 0 "R202" V 8130 3550 50  0000 C CNN
F 1 "300" V 8050 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 7980 3550 50  0001 C CNN
F 3 "" H 8050 3550 50  0001 C CNN
	1    8050 3550
	0    1    1    0   
$EndComp
$Comp
L R R203
U 1 1 5B040510
P 8050 3650
F 0 "R203" V 8130 3650 50  0000 C CNN
F 1 "300" V 8050 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 7980 3650 50  0001 C CNN
F 3 "" H 8050 3650 50  0001 C CNN
	1    8050 3650
	0    1    1    0   
$EndComp
$Comp
L R R204
U 1 1 5B040577
P 8050 3800
F 0 "R204" V 8130 3800 50  0000 C CNN
F 1 "300" V 8050 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 7980 3800 50  0001 C CNN
F 3 "" H 8050 3800 50  0001 C CNN
	1    8050 3800
	0    1    1    0   
$EndComp
$Comp
L R R205
U 1 1 5B0405E3
P 8050 3900
F 0 "R205" V 8130 3900 50  0000 C CNN
F 1 "300" V 8050 3900 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 7980 3900 50  0001 C CNN
F 3 "" H 8050 3900 50  0001 C CNN
	1    8050 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 3550 8350 3550
Wire Wire Line
	8350 3650 8200 3650
Wire Wire Line
	8200 3800 8350 3800
Wire Wire Line
	8350 3900 8200 3900
Text GLabel 6450 2850 2    60   Input ~ 0
POWER_EN
$Comp
L +3.3V #PWR048
U 1 1 5B0478B4
P 6400 2250
F 0 "#PWR048" H 6400 2100 50  0001 C CNN
F 1 "+3.3V" H 6400 2390 50  0000 C CNN
F 2 "" H 6400 2250 50  0001 C CNN
F 3 "" H 6400 2250 50  0001 C CNN
	1    6400 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2250 6250 2250
$Comp
L GND #PWR049
U 1 1 5B047DA2
P 4050 1950
F 0 "#PWR049" H 4050 1700 50  0001 C CNN
F 1 "GND" H 4050 1800 50  0000 C CNN
F 2 "" H 4050 1950 50  0001 C CNN
F 3 "" H 4050 1950 50  0001 C CNN
	1    4050 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 1950 4150 1950
$Comp
L R R206
U 1 1 5B049656
P 8400 1250
F 0 "R206" V 8480 1250 50  0000 C CNN
F 1 "R" V 8400 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 8330 1250 50  0001 C CNN
F 3 "" H 8400 1250 50  0001 C CNN
	1    8400 1250
	1    0    0    -1  
$EndComp
$Comp
L R R207
U 1 1 5B0496AB
P 8550 1250
F 0 "R207" V 8630 1250 50  0000 C CNN
F 1 "R" V 8550 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 8480 1250 50  0001 C CNN
F 3 "" H 8550 1250 50  0001 C CNN
	1    8550 1250
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR050
U 1 1 5B049D19
P 8550 1000
F 0 "#PWR050" H 8550 850 50  0001 C CNN
F 1 "+3.3V" H 8550 1140 50  0000 C CNN
F 2 "" H 8550 1000 50  0001 C CNN
F 3 "" H 8550 1000 50  0001 C CNN
	1    8550 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1000 8550 1100
Wire Wire Line
	8550 1050 8400 1050
Wire Wire Line
	8400 1050 8400 1100
Connection ~ 8550 1050
Text Label 5250 3750 3    60   ~ 0
PCNT0_A
Text Label 5350 3750 3    60   ~ 0
PCNT0_B
Text Label 4150 2650 2    60   ~ 0
PCNT1_A
Text Label 4150 2750 2    60   ~ 0
PCNT1_B
Wire Wire Line
	7800 3550 7900 3550
Wire Wire Line
	7900 3650 7800 3650
Wire Wire Line
	7800 3800 7900 3800
Wire Wire Line
	7900 3900 7800 3900
Text Label 7800 3650 2    60   ~ 0
PCNT1_A
Text Label 7800 3550 2    60   ~ 0
PCNT1_B
Wire Wire Line
	6250 2550 6450 2550
Wire Wire Line
	6450 2650 6250 2650
Wire Wire Line
	6250 2750 6450 2750
Wire Wire Line
	6450 2850 6250 2850
Text Label 7800 3800 2    60   ~ 0
PCNT0_B
Text Label 7800 3900 2    60   ~ 0
PCNT0_A
Wire Wire Line
	5250 3750 5250 3650
Wire Wire Line
	5350 3650 5350 3750
Wire Wire Line
	5900 3700 5750 3700
Wire Wire Line
	5750 3700 5750 3650
Wire Wire Line
	5650 3650 5650 3800
Wire Wire Line
	5650 3800 5900 3800
Wire Wire Line
	6250 3050 6450 3050
Wire Wire Line
	6450 3150 6250 3150
Wire Wire Line
	5900 3900 5550 3900
Wire Wire Line
	5550 3900 5550 3650
$Comp
L C C212
U 1 1 5B05E626
P 7000 1750
F 0 "C212" H 7025 1850 50  0000 L CNN
F 1 "C" H 7025 1650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7038 1600 50  0001 C CNN
F 3 "" H 7000 1750 50  0001 C CNN
	1    7000 1750
	0    1    1    0   
$EndComp
$Comp
L C C216
U 1 1 5B05E6B8
P 7000 2150
F 0 "C216" H 7025 2250 50  0000 L CNN
F 1 "C" H 7025 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7038 2000 50  0001 C CNN
F 3 "" H 7000 2150 50  0001 C CNN
	1    7000 2150
	0    1    1    0   
$EndComp
Connection ~ 6650 1750
Connection ~ 6650 2150
Wire Wire Line
	7150 1750 7350 1750
Wire Wire Line
	7350 1750 7350 2150
Wire Wire Line
	7350 2150 7150 2150
Wire Wire Line
	7350 1950 7450 1950
Connection ~ 7350 1950
$Comp
L GND #PWR051
U 1 1 5B05EB37
P 7450 1950
F 0 "#PWR051" H 7450 1700 50  0001 C CNN
F 1 "GND" H 7450 1800 50  0000 C CNN
F 2 "" H 7450 1950 50  0001 C CNN
F 3 "" H 7450 1950 50  0001 C CNN
	1    7450 1950
	0    -1   -1   0   
$EndComp
Text HLabel 8400 1400 3    60   Output ~ 0
SDA
Text HLabel 8550 1400 3    60   BiDi ~ 0
SCL
Text HLabel 6450 2750 2    60   Input ~ 0
LASERS_INT
Text Notes 4600 1300 0    60   ~ 0
I2C1
Text Notes 3300 2300 0    60   ~ 0
USART0
Text Notes 5300 4500 1    60   ~ 0
PCNT0
Text Notes 3400 2700 0    60   ~ 0
PCNT1
Wire Wire Line
	5450 3650 5450 4000
Wire Wire Line
	5450 4000 5900 4000
Text Notes 7050 3100 0    60   ~ 0
USART1
Text Notes 6400 3750 0    60   ~ 0
I2C0
Wire Wire Line
	3950 2550 4150 2550
Wire Wire Line
	4150 2450 3950 2450
Wire Wire Line
	6250 2950 6450 2950
Wire Wire Line
	1200 4750 950  4750
Wire Wire Line
	950  4300 1200 4300
$EndSCHEMATC
